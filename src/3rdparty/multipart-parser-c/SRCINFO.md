
homepage: [multipart-parser-c GitHub page](https://github.com/iafonov/multipart-parser-c)
used version: [commit 772639cf10db6d9f5a655ee9b7eb20b815fab396](https://github.com/iafonov/multipart-parser-c/tree/772639cf10db6d9f5a655ee9b7eb20b815fab396)

* applied [#24](https://github.com/iafonov/multipart-parser-c/pull/24)
* applied [#29](https://github.com/iafonov/multipart-parser-c/pull/29)

* missing [#15](https://github.com/iafonov/multipart-parser-c/pull/15)
* missing [#25](https://github.com/iafonov/multipart-parser-c/pull/25)
* missing [#28](https://github.com/iafonov/multipart-parser-c/pull/28) - It messes up the indexing.
* missing [#32](https://github.com/iafonov/multipart-parser-c/pull/32)
