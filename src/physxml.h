#ifndef __ISDS_PHYSXML_H__
#define __ISDS_PHYSXML_H__

#include "libdatovka/isds.h"

#define PHYSXML_ELEMENT_SEPARATOR "|"
#define PHYSXML_NS_SEPARATOR ">"

/* Check for expat compile-time configuration
 * @current_version is static string describing current expat version */
isds_error _isds_init_expat(const char **current_version);

/* Locate element specified by element path in XML stream.
 * TODO: Support other encodings than UTF-8
 * @document is XML document as bit stream
 * @length is size of @document in bytes. Zero length is forbidden.
 * @path is special path (e.g. "|html|head|title",
 * qualified element names are specified as
 * NSURI '>' LOCALNAME, omit NSURI and '>' separator if no namespace
 * should be addressed (i.e. use only locale name)
 * You can use PHYSXML_ELEMENT_SEPARATOR and PHYSXML_NS_SEPARATOR string
 * macros.
 * @start outputs start of the element location in @document (inclusive,
 * counts from 0)
 * @end outputs end of element (inclusive, counts from 0)
 * @return 0 if element found */
isds_error _isds_find_element_boundary(void *document, size_t length,
        char *path, size_t *start, size_t *end);
#endif
