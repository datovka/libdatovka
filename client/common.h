#ifndef __COMMON_H__
#define __COMMON_H__

#include <libdatovka/isds.h>

#define url isds_testing_locator
const char *username(void);
const char *password(void);

void print_bool(const _Bool *boolean);
void print_longint(const long int *number);
void print_ulongint(const unsigned long int *number);
void print_isds_status(const struct isds_status *status);
void print_DbState(const long int state);
void print_date(const struct tm *date);
void print_isds_status(const struct isds_status *status);
void print_DbOwnerInfo(const struct isds_DbOwnerInfo *info);
void print_DbOwnerInfoExt2(const struct isds_DbOwnerInfoExt2 *info);
void print_DbUserInfo(const struct isds_DbUserInfo *info);
void print_DbUserInfoExt2(const struct isds_DbUserInfoExt2 *info);
void print_DTInfoOutput(const struct isds_DTInfoOutput *info);
void print_timeval(const struct isds_timeval *time);
void print_hash(const struct isds_hash *hash);
void print_envelope(const struct isds_envelope *envelope);
void print_dmFile(const struct isds_dmFile *dm_file);
void print_dmAtt(const struct isds_dmAtt *dm_att);
void print_message(const struct isds_message *message);
void print_copies(const struct isds_list *copies);
void print_message_status_change(
    const struct isds_message_status_change *changed_status);
void print_sender_type(const isds_sender_type *type);

void print_dmMessageAuthor(struct isds_dmMessageAuthor *author);

void compare_hashes(const struct isds_hash *hash1,
    const struct isds_hash *hash2);
int progressbar_int(int64_t upload_total, int64_t upload_current,
    int64_t download_total, int64_t download_current,
    void *data);
int progressbar(double upload_total, double upload_current,
    double download_total, double download_current,
    void *data);

int mmap_file(const char *file, int *fd, void **buffer, size_t *length);
int munmap_file(int fd, void *buffer, size_t length);

int save_data_to_file(const char *message, const char *file,
    const void *data, const size_t length);
int save_data(const char *message, const void *data, const size_t length);

int file_cmp(const char *file1, const char *file2);

#endif
