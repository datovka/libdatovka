#include "../config.h"
#define _XOPEN_SOURCE XOPEN_SOURCE_LEVEL_FOR_STRDUP
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <time.h>
#include <string.h>
#include <libdatovka/isds.h>
#include "common.h"


int main(void) {
    struct isds_ctx *ctx = NULL, *request_ctx = NULL;
    isds_error err;
    struct isds_DbOwnerInfo *db_owner_info = NULL;
    struct isds_list *users = NULL;

    setlocale(LC_ALL, "");

    err = isds_init();
    if (err) {
        printf("isds_init() failed: %s\n", isds_strerror(err));
        exit(EXIT_FAILURE);
    }

    isds_set_logging(ILF_ALL & ~ILF_HTTP, ILL_ALL);

    ctx = isds_ctx_create();
    if (!ctx) {
        printf("isds_ctx_create() failed");
    }

    err = isds_set_timeout(ctx, 10000);
    if (err) {
        printf("isds_set_timeout() failed: %s\n", isds_strerror(err));
    }

    err = isds_login(ctx, url, username(), password(), NULL, NULL);
    if (err) {
        printf("isds_login() failed: %s: %s\n", isds_strerror(err),
                isds_long_message(ctx));
    } else {
        printf("Logged in :)\n");
    }


    {
        printf("Getting info about my box:\n");
        err = isds_GetOwnerInfoFromLogin(ctx, &db_owner_info);
        if (err) {
            printf("isds_GetOwnerInfoFromLogin() failed: %s: %s\n",
                    isds_strerror(err), isds_long_message(ctx));
        } else {
            printf("isds_GetOwnerInfoFromLogin() succeeded\n");
        }
        print_DbOwnerInfo(db_owner_info);

    }

    /* Get info all users of this box */
    if (db_owner_info) {
        struct isds_list *item;
        printf("Getting users of my box with ID `%s':\n", db_owner_info->dbID);
        err = isds_GetDataBoxUsers(ctx, db_owner_info->dbID, &users);
        if (err) {
            printf("isds_GetDataBoxUsers() failed: %s: %s\n",
                    isds_strerror(err), isds_long_message(ctx));
        } else {
            printf("isds_GetDataBoxUsers() succeeded\n");
            for(item = users; item; item = item->next) {
                printf("List item:\n");
                print_DbUserInfo(item->data);
            }
        }
    }


    /* Request for creating the same box */
    if (db_owner_info) {
        char *refnumber = NULL;
        const struct isds_approval approval = {
            .approved = 1, .reference = "Me"
        };

        free(db_owner_info->email);
        db_owner_info->email = strdup("franta@example.com");

        request_ctx = isds_ctx_create();
        if (!request_ctx) {
            printf("isds_ctx_create() failed");
        }

        printf("Requesting to create already existing box\n");
        isds_set_logging(ILF_ALL, ILL_ALL);
        err = isds_request_new_testing_box(request_ctx, db_owner_info, users,
                "Former Names", &approval, &refnumber);
        isds_set_logging(ILF_ALL & ~ILF_HTTP, ILL_ALL);
        if (err) {
            printf("isds_request_new_testing_box() failed: %s: %s\n",
                    isds_strerror(err), isds_long_message(request_ctx));
        } else {
            printf("isds_request_new_testing_box() succeeded as request #%s: "
                    "new box ID: %s\n",
                    refnumber, db_owner_info->dbID);
        }

        err = isds_ctx_free(&request_ctx);
        if (err) {
            printf("isds_ctx_free() failed: %s\n", isds_strerror(err));
        }
    }

    isds_DbOwnerInfo_free(&db_owner_info);
    isds_list_free(&users);


    err = isds_logout(ctx);
    if (err) {
        printf("isds_logout() failed: %s\n", isds_strerror(err));
    }


    err = isds_ctx_free(&ctx);
    if (err) {
        printf("isds_ctx_free() failed: %s\n", isds_strerror(err));
    }


    err = isds_cleanup();
    if (err) {
        printf("isds_cleanup() failed: %s\n", isds_strerror(err));
    }

    exit (EXIT_SUCCESS);
}
