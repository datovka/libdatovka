#ifndef __COMMON_H__
#define __COMMON_H__

const char *username(void);
const char *password(void);

const char *username_mep(void);
const char *code_mep(void);

#endif
